from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.contrib import admin
from .views import (login_view, register_view, logout_view, change_password)

urlpatterns = [
    url(r'^login/$',login_view, name='login'),
    url(r'^register/$', register_view, name='register'),
    url(r'^password/change/$', change_password, name='change_password'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

]

