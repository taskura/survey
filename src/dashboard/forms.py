from django.forms import ModelForm, inlineformset_factory
from excercise.models import Excercise,Project,Question,AssignExcercise


class ProjectForm(ModelForm):
	class Meta:
		model = Project
		exclude = ['created_by']	

class ExcerciseForm(ModelForm):
	class Meta:
		model = Excercise
		exclude = ['created_by']

class QuestionForm(ModelForm):
	class Meta:
		model = Question
		fields = '__all__'



class AssignExcerciseForm(ModelForm):
	class Meta:
		model = AssignExcercise
		fields = '__all__'