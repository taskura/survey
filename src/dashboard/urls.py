from django.conf.urls import url
from django.contrib.admin.views.decorators import staff_member_required
from .views import (
		DbHome,
		ProjectList,
		ExcerciseList,
		QuestionList,
		AssignExcerciseList,
		ProjectCreate,
		ExcerciseCreate,
		QuestionCreate,
		AssignExcerciseCreate,
		ProjectUpdate,
		ExcerciseUpdate,
		QuestionUpdate,
		AssignExcerciseUpdate,
		project_delete,
		excercise_delete,
		question_delete,
		assignexcercise_delete,
		AssignExcerciseDetails
	)

urlpatterns = [
    url(r'^dashboard/$', DbHome, name='DbHome'),
    url(r'^dashboard/project/list/$',  staff_member_required(ProjectList.as_view()), name='project_List'),
    url(r'^dashboard/excercise/list/$',  staff_member_required(ExcerciseList.as_view()), name='excercise_list'),
    url(r'^dashboard/question/list/$',  staff_member_required(QuestionList.as_view()), name='question_list'),
    url(r'^dashboard/assign/list/$',  staff_member_required(AssignExcerciseList.as_view()), name='assign_list'),
    url(r'^dashboard/project/create/$',  staff_member_required(ProjectCreate.as_view()), name='project_create'),
    url(r'^dashboard/excercise/create/$',  staff_member_required(ExcerciseCreate.as_view()), name='excercise_create'),
    url(r'^dashboard/question/create/$',  staff_member_required(QuestionCreate.as_view()), name='question_create'),
    url(r'^dashboard/assign/create/$',  staff_member_required(AssignExcerciseCreate.as_view()), name='assign_excercise_create'),
    url(r'^dashboard/project/(?P<pk>[\d]+)/update/$',  staff_member_required(ProjectUpdate.as_view()), name='project_update'),
    url(r'^dashboard/excercise/(?P<pk>[\d]+)/update/$',  staff_member_required(ExcerciseUpdate.as_view()), name='excercise_update'),
    url(r'^dashboard/question/(?P<pk>[\d]+)/update/$',  staff_member_required(QuestionUpdate.as_view()), name='question_update'),
    url(r'^dashboard/assign/(?P<pk>[\d]+)/update/$',  staff_member_required(AssignExcerciseUpdate.as_view()), name='assign_update'),
    url(r'^dashboard/assign/(?P<pk>[\d]+)/details/$',  staff_member_required(AssignExcerciseDetails.as_view()), name='assign_details'),
    url(r'^dashboard/project/(?P<pk>[\d]+)/delete/$', project_delete, name='project_delete'),
    url(r'^dashboard/excercise/(?P<pk>[\d]+)/delete/$', excercise_delete, name='excercise_delete'),
    url(r'^dashboard/question/(?P<pk>[\d]+)/delete/$', question_delete, name='question_delete'),
    url(r'^dashboard/assign/(?P<pk>[\d]+)/delete/$', assignexcercise_delete, name='assignexcercise_delete')

]