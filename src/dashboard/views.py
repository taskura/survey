from django.shortcuts import render,redirect

# Create your views here.
from excercise.models import Project,Question,Excercise,AssignExcercise
from django.views.generic import ListView,CreateView,UpdateView
from .forms import ExcerciseForm,ProjectForm,QuestionForm,AssignExcerciseForm
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView


@staff_member_required
def DbHome(request):
	return render(request,"dashboard/home.html")


class ProjectList(LoginRequiredMixin, ListView):
	model = Project
	template_name = 'dashboard/project/projectlist.html'
	context_object_name = 'project_list'


class ExcerciseList(LoginRequiredMixin, ListView):
	model = Excercise
	template_name = 'dashboard/excercise/excercise_list.html'
	context_object_name = 'excercise_list'

class QuestionList(LoginRequiredMixin, ListView):
	model = Question
	template_name = 'dashboard/question/question_list.html'
	context_object_name = 'question_list'

class AssignExcerciseList(LoginRequiredMixin, ListView):
	model = AssignExcercise
	template_name = 'dashboard/assign/assign_list.html'
	context_object_name = 'assign_list'


class AssignExcerciseDetails(LoginRequiredMixin, DetailView):
	model = AssignExcercise
	template_name = 'dashboard/assign/assign_details.html'
	context_object_name = 'assign'


class ProjectCreate(LoginRequiredMixin, CreateView):
	model = Project
	template_name = 'dashboard/project/create_project.html'
	form_class = ProjectForm
	success_url = '/dashboard/project/list/'


class ExcerciseCreate(LoginRequiredMixin, CreateView):
	model = Excercise
	template_name = 'dashboard/excercise/create_excercise.html'
	form_class = ExcerciseForm
	success_url = '/dashboard/excercise/list/'



class QuestionCreate(LoginRequiredMixin, CreateView):
	model = Question
	template_name = 'dashboard/question/create_question.html'
	form_class = QuestionForm
	success_url = '/dashboard/question/list/'


class AssignExcerciseCreate(LoginRequiredMixin, CreateView):
	model = AssignExcercise
	template_name = 'dashboard/assign/create_assign.html'
	form_class = AssignExcerciseForm
	success_url = '/dashboard/assign/list/'


class ProjectUpdate(LoginRequiredMixin, UpdateView):
	model = Project
	template_name = 'dashboard/project/create_project.html'
	form_class = ProjectForm
	success_url = '/dashboard/project/list/'


class ExcerciseUpdate(LoginRequiredMixin, UpdateView):
	model = Excercise
	template_name = 'dashboard/excercise/create_excercise.html'
	form_class = ExcerciseForm
	success_url = '/dashboard/excercise/list/'

class QuestionUpdate(LoginRequiredMixin, UpdateView):
	model = Question
	template_name = 'dashboard/question/create_question.html'
	form_class = QuestionForm
	success_url = '/dashboard/question/list/'

class AssignExcerciseUpdate(LoginRequiredMixin, UpdateView):
	model = AssignExcercise
	template_name = 'dashboard/assign/create_assign.html'
	form_class = AssignExcerciseForm
	success_url = '/dashboard/assign/list/'

@staff_member_required
def project_delete(request,pk):
	obj = Project.objects.get(id=pk)
	obj.delete()
	return redirect('/dashboard/project/list/')

@staff_member_required
def excercise_delete(request,pk):
	obj = Excercise.objects.get(id=pk)
	obj.delete()
	return redirect('/dashboard/excercise/list/')

@staff_member_required
def question_delete(request,pk):
	obj = Question.objects.get(id=pk)
	obj.delete()
	return redirect('/dashboard/question/list/')	
@staff_member_required
def assignexcercise_delete(request,pk):
	obj = AssignExcercise.objects.get(id=pk)
	obj.delete()
	return redirect('/dashboard/assign/list/')