# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
	Project, 
	Excercise, 
	Question, 
	AssignExcercise, 
	LastTakeQuestion,
	NumericRadioFormat,
	ChoiceItemFormat,
	BinaryFormat,
	QuestionAnswer
	)
# Register your models here.
admin.site.register(Project)
admin.site.register(Excercise)
admin.site.register(Question)
admin.site.register(AssignExcercise)
admin.site.register(LastTakeQuestion)
admin.site.register(NumericRadioFormat)
admin.site.register(ChoiceItemFormat)
admin.site.register(BinaryFormat)
admin.site.register(QuestionAnswer)