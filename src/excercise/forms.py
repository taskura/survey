from django.forms import ModelForm

from .models import QuestionAnswer

class QuestionAnswerForm(ModelForm):
	class Meta:
		model = QuestionAnswer
		exclude = ()

