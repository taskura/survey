# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_save, post_save
# Create your models here.

class Project(models.Model):
	project_name = models.CharField(max_length = 100)
	created_by 	 = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
	created_at   = models.DateTimeField(auto_now = True,auto_now_add = False)
	updated      = models.DateTimeField(auto_now = False,auto_now_add = True)

	def __str__(self):
		return self.project_name


class Excercise(models.Model):
	project 	   = models.ForeignKey(Project,on_delete=models.CASCADE,null = True,blank = True)
	excercise_name = models.CharField(max_length = 100)
	created_by     = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
	created_at     = models.DateTimeField(auto_now = True,auto_now_add = False)
	updated        = models.DateTimeField(auto_now = False,auto_now_add = True)

	def __str__(self):
		return self.excercise_name

ANSWER_CHOICE = (
    ('Binary', 'Binary'),
    ('Numeric', 'Numeric'),
    ('Choice', 'Choice Filed'),
    ('Textbox', 'Textbox')
)

class NumericRadioFormat(models.Model):
	title = models.CharField(max_length=50)
	numeric1 = models.CharField(max_length=50, null=True, blank=True)
	numeric2 = models.CharField(max_length=50, null=True, blank=True)
	numeric3 = models.CharField(max_length=50, null=True, blank=True)
	numeric4 = models.CharField(max_length=50, null=True, blank=True)
	numeric5 = models.CharField(max_length=50, null=True, blank=True)
	numeric6 = models.CharField(max_length=50, null=True, blank=True)
	numeric7 = models.CharField(max_length=50, null=True, blank=True)
	numeric8 = models.CharField(max_length=50, null=True, blank=True)
	numeric9 = models.CharField(max_length=50, null=True, blank=True)
	numeric10 = models.CharField(max_length=50, null=True, blank=True)

	def __str__(self):
		return self.title

class ChoiceItemFormat(models.Model):
	title = models.CharField(max_length=50)
	item1 = models.CharField(max_length=50, null=True, blank=True)
	item2 = models.CharField(max_length=50, null=True, blank=True)
	item3 = models.CharField(max_length=50, null=True, blank=True)

	def __str__(self):
		return self.title

class BinaryFormat(models.Model):
	title = models.CharField(max_length=50)
	item1 = models.CharField(max_length=50, null=True, blank=True)
	item2 = models.CharField(max_length=50, null=True, blank=True)

	def __str__(self):
		return self.title

class Question(models.Model):
	excercise 	= models.ForeignKey(Excercise,on_delete=models.CASCADE)
	title 		= models.CharField(max_length = 150)
	description = models.TextField()
	answer_format = models.CharField(max_length=20,choices=ANSWER_CHOICE)
	numeric_format = models.ForeignKey(NumericRadioFormat,on_delete=models.CASCADE,null = True,blank = True)
	choice_item_format = models.ForeignKey(ChoiceItemFormat,on_delete=models.CASCADE,null = True,blank = True)
	binary_format = models.ForeignKey(BinaryFormat,on_delete=models.CASCADE,null = True,blank = True)
	additional_box = models.BooleanField(default=False)
	def __str__(self):
		return self.title


class AssignExcercise(models.Model):
	user         = models.ForeignKey(User,on_delete=models.CASCADE)
	excercise 	 = models.ForeignKey(Excercise,on_delete=models.CASCADE)
	assign_at    = models.DateTimeField(auto_now = True,auto_now_add = False)
	answered = models.BooleanField(default=False)
	def __str__(self):
		return self.user.username

def take_q_receiver(sender,created, instance, *args, **kwargs):
	if created:
		LastTakeQuestion.objects.create(assign=instance)
post_save.connect(take_q_receiver, sender=AssignExcercise)


class LastTakeQuestion(models.Model):
	assign = models.OneToOneField(AssignExcercise,on_delete=models.CASCADE, related_name="lqt")
	page = models.IntegerField(default=1)

	def __str__(self):
		return str(self.id)



class QuestionAnswer(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	assign = models.ForeignKey(AssignExcercise, on_delete=models.CASCADE, related_name="answerq")
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	answer = models.TextField(null=True,blank=True)
	additional_answer = models.TextField(null=True,blank=True)
	answer_at = models.DateTimeField(auto_now = True,auto_now_add = False)
	

	def __str__(self):
		return self.question.title