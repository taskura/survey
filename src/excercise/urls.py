from django.conf.urls import url
from .views import excercise_details, excercise_details, AnswerSave, AnswerUpdate

urlpatterns = [
    url(r'^excercise/(?P<id>[\d]+)/$', excercise_details, name='excercise-details'),
    url(r'^question/answer/$', AnswerSave, name='question-answer'),
    url(r'^question/answer/(?P<id>[\d]+)/update/$', AnswerUpdate, name='question-answer-update'),

]