# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.shortcuts import render,redirect
from django.views.generic import ListView, DetailView
from .models import Project,Excercise,Question, AssignExcercise, LastTakeQuestion,QuestionAnswer
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from .forms import QuestionAnswerForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.



@login_required
def Home(request):
	if request.user.is_staff:
		return redirect("/dashboard/")
	context = {
		'assign_excercise': AssignExcercise.objects.filter(user=request.user,answered=False)
	}
	return render(request,'home.html', context)


@login_required
def excercise_details(request,id):
	assign_obj = AssignExcercise.objects.get(id=id)
	excercise_obj = Excercise.objects.get(id=assign_obj.excercise.id)
	try:
		last_question = LastTakeQuestion.objects.get(assign=assign_obj).page
	except Exception as e:
		last_question = 1

	question = Question.objects.filter(excercise=excercise_obj)
	page = request.GET.get('question', last_question)
	paginator = Paginator(question, 1)
	page1 = paginator.page(page)
	print(page1)
	q = page1.object_list[0].id

	try:
		ans_obj = QuestionAnswer.objects.get(assign=assign_obj,question__id=q, user=request.user)
	except Exception as e:
		ans_obj = None

	if ans_obj:
		ansform = QuestionAnswerForm(instance=ans_obj)
	else:
		ansform = QuestionAnswerForm()

	try:
		ex_page = paginator.page(page)
	except PageNotAnInteger:
		ex_page = paginator.page(1)
	except EmptyPage:
		ex_page = paginator.page(paginator.num_pages)

	total = paginator.count
	current = page
	completep = int(current) / int(total)  * 100
	context = {
		'assign_excercise':AssignExcercise.objects.filter(user=request.user),
		'ex_page': ex_page,
		'ansform':ansform,
		'assign_obj':assign_obj,
		'total':total,
		'current':current,
		'completep':int(round(completep/10.0)*10.0),
		'today':datetime.today()
	}
	return render(request, 'test.html',context)


def AnswerSave(request):
	data = dict()
	if request.method == 'POST':
		user = request.POST['user']
		question = request.POST['question']
		assign = request.POST['assign']
		additional_answer = request.POST['additional_answer']
		answer = request.POST['answer']
		crnt_page = request.POST['crnt_page']
		
		form = QuestionAnswerForm(request.POST)
		if form.is_valid():
			form.user = user
			form.assign = assign
			form.question = question
			form.answer = answer
			form.additional_answer = additional_answer
			form.save()

		last_q = LastTakeQuestion.objects.get(assign=int(assign))
		last_q.page = int(crnt_page)
		last_q.save()


	return JsonResponse(data)

def AnswerUpdate(request,id):
	qanswer = QuestionAnswer.objects.get(id=id)
	data = dict()
	if request.method == 'POST':
		user = request.POST['user']
		question = request.POST['question']
		assign = request.POST['assign']
		additional_answer = request.POST['additional_answer']
		answer = request.POST['answer']
		nexturl = request.POST['nexturl']

		qanswer.answer = answer
		qanswer.additional_answer = additional_answer
		qanswer.save()

		if nexturl == "/":
			assign_ob = AssignExcercise.objects.get(id=qanswer.assign.id)
			assign_ob.answered = True
			assign_ob.save()

	return JsonResponse(data)
