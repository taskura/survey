
$(document).ready(function() {
    $("#ans_form_id").submit(function(event){
      var btn = $(this);
      event.preventDefault();
      var id_user = $("#id_user").val()
      var id_question = $("#id_question").val()
      var id_assign = $("#id_assign").val()

      var id_additional_answer = $("#id_additional_answer").val()

      if (id_additional_answer == undefined){
        id_additional_answer = ""
      }


      var id_answer_format = $("#id_answer_format").val()

      if (id_answer_format == "Numeric"){
        var id_answer = $("#numeric_value").val()
      }else if(id_answer_format == "Binary"){
        var id_answer = $("#binary_value").val()
      }else if(id_answer_format == "Choice"){
        var id_answer = $("#choice_value").val()
      }else{
        var id_answer = $("#id_answer").val()
      }
     
      
      var crnt_page = $("#id_crnt_page").val()

      var nexturl =  $("#nexturl").val()

    $.ajax({
      type:'POST',
      url:btn.attr("action"),
      data:{
        answer:id_answer,
        user:id_user,
        question:id_question,
        assign:id_assign,
        additional_answer:id_additional_answer,
        crnt_page:crnt_page,
        nexturl:nexturl,
        csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
      },
      success:function(data){
        window.location.replace(nexturl);
      },

    });



  });


  });